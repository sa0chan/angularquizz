// faire en sorte que l'application fonctionne  + api de lancée
//it mot clé pour test
it('Test list questions -affichage ', () =>{
  cy.visit('http://localhost:4200/questions')
  cy.get('app-list-question li').should('have.length', 1)
  //cy.get('app-list-question li p').eq(0).contains('Comment s\'appelle le chien d\'Obélix ?')
  cy.get('app-list-question li button').eq(1)
});

it('Test list questios - répondre à une question',()=>{
  cy.visit('http://localhost:4200/questions')
  cy.get('app-list-question li button').eq(1).click()

})
it('test input question ',  () => {
  cy.visit('http://localhost:4200/questions')
  //robot ecrit
  cy.get('app-list-question li input').type('Sarah')
  //robot clic
  cy.get('app-list-question li button').eq(1).click()
  cy.get('app-list-question li p').eq(1).contains('Trop doué')
});
