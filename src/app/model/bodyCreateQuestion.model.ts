export class BodyCreateQuestion {
  enonce: string;
  suggestions: string[];
  type: string;
  reponse: string ;

  constructor( enonce: string, suggestions: string[], reponse: string, type: string  ) {
  this.reponse = reponse;
  this.enonce = enonce ;
  this.suggestions = suggestions;
  this.type = type;
  }
}
