export class Question {
  id: number;
  enonce: string;
  suggestions: string;
  result: string;
  type: string;

  constructor(id: number, enonce: string, suggestions: string, type: string  ) {
    this.id = id;
    this.enonce = enonce ;
    this.suggestions = suggestions;
    this.type = type;
  }
}
