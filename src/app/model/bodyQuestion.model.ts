export class BodyQuestion {
  answer: string;
  result: string;

  constructor(answer: string) {
    this.answer = answer;
  }
}
