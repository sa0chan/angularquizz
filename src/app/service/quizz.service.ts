import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Question} from '../model/question.model';
import {BodyQuestion} from '../model/bodyQuestion.model';


@Injectable()
export class QuizzService {

  constructor(private http: HttpClient) {
  }

  // Observable
  getQuestionsList(): Observable<Question[]> {
    return this.http.get<Question[]>('http://localhost:8080/questions');
  }

  postUserAnswer(id: number, body: BodyQuestion): Observable<Question> {
    return this.http.post<Question>(`http://localhost:8080/questions/${id}/answer`, body);
  }

  postNewQuestion(body): Observable<Question> {
    return this.http.post<Question>('http://localhost:8080/questions', body);
  }

  deleteQuestion(id): Observable<Question> {
    return this.http.delete<Question>(`http://localhost:8080/questions/${id}/`);
  }
}

