import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListQuestionComponent } from './component/list-question/list-question.component';
import { CreateQuestionComponent } from './component/create-question/create-question.component';
import {HttpClientModule} from '@angular/common/http';
import {QuizzService} from './service/quizz.service';
import { QuestionComponent } from './component/question/question.component';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  {path : 'questions', component : ListQuestionComponent},
  {path : 'create', component : CreateQuestionComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    ListQuestionComponent,
    CreateQuestionComponent,
    QuestionComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    ),

  ],
  // services
  providers: [QuizzService],
  bootstrap: [AppComponent]
})
export class AppModule { }
