import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../model/question.model';
import {QuizzService} from '../../service/quizz.service';
import {BodyQuestion} from '../../model/bodyQuestion.model';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  @Input() question: Question;
  @Input() index: number;

  @Output() updatedList = new EventEmitter<Question[]>();

  id: number;
  userReponse: string;
  jsonAnswerBody: BodyQuestion;
  result: string;
  textResult: string;

  enregistrerUserResponse(id: number) {
    this.id = id;
    this.jsonAnswerBody = new BodyQuestion(this.userReponse);
    this.questionService.postUserAnswer(this.id, this.jsonAnswerBody).subscribe(result => {
      this.result = result.result;
      this.getResult();
    });
  }

  getResult() {
    console.log(this.result);
    if (this.result === 'ALMOST_CORRECT') {
      this.textResult = 'Pas mal';
    } else if (this.result === 'CORRECT') {
      this.textResult = 'Trop doué';
    } else if (this.result === 'INCORRECT') {
      this.textResult = 'mauvaise réponse';
    }
  }

  deleteQuestion(id) {
    this.questionService.deleteQuestion(id).subscribe(() => {
      this.reloadListQuestion();
    });
  }

  reloadListQuestion() {
    this.questionService.getQuestionsList().subscribe(response => {
      console.log('rentre le getquestionlist');
      console.log(response);
      // ici on dit que l'ont va emettre
      this.updatedList.emit(response);

    });
  }

  constructor(private questionService: QuizzService) {
  }


  ngOnInit() {
  }

}
