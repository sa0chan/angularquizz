import {Component, OnInit} from '@angular/core';
import {QuizzService} from '../../service/quizz.service';
import {BodyCreateQuestion} from '../../model/bodyCreateQuestion.model';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {
  selectedType = 'TRUE_FALSE';
  questionText: string;
  responseText: string;
  multiChoiceList: string[] = [];
  multiChoice1: string;
  multiChoice2: string;
  multiChoice3: string;

  constructor(private questionService: QuizzService) {
  }

  creerLaQuestion() {
    this.multiChoiceList.push(this.multiChoice1);
    this.multiChoiceList.push(this.multiChoice2);
    this.multiChoiceList.push(this.multiChoice3);
    if (this.selectedType === 'TRUE_FALSE') {
      this.responseText = JSON.parse(this.responseText);
    }
    const body = new BodyCreateQuestion(this.questionText, this.multiChoiceList, this.responseText, this.selectedType);
    this.questionService.postNewQuestion(body).subscribe();
  }

  ngOnInit() {
  }

}
