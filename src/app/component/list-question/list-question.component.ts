import {Component, Input, OnInit} from '@angular/core';
import {QuizzService} from '../../service/quizz.service';
import {Question} from '../../model/question.model';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {
 @Input() questionList: Question[] = [];
  constructor(private questionService: QuizzService) { }

  onQuestionDeleted(eventEmitted: Question[]) {
    console.log('ceci est un event');
    console.log(eventEmitted);
    this.questionList = eventEmitted ;
  }

  ngOnInit() {
    this.questionService.getQuestionsList().subscribe(response => {
      response.forEach(question => {
          this.questionList.push(question);
      });
    });
  }

}
